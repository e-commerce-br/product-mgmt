package com.diego.ecbr.productmgmt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcbrProductMgmtApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcbrProductMgmtApplication.class, args);
	}

}
