package com.diego.ecbr.productmgmt.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.diego.ecbr.productmgmt.rest.SampleRestController.SAMPLE_API;

@RestController
@RequestMapping(SAMPLE_API)
public class SampleRestController {

    public final static String SAMPLE_API = "/api/sample";
    private final static String WELCOME_MSG = "Welcome to E-Commerce-br";

    @GetMapping
    public String getWelcomeMsg(){
        return WELCOME_MSG;
    }
}
