package com.diego.ecbr.productmgmt.rest;

import com.diego.ecbr.productmgmt.dao.model.Category;
import com.diego.ecbr.productmgmt.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static com.diego.ecbr.productmgmt.rest.CategoryRestController.CATEGORY_API;
@RestController
@RequestMapping(CATEGORY_API)
public class CategoryRestController {

    public static final String CATEGORY_API = "api/categories";

    @Autowired
    private CategoryService categoryService;

    @GetMapping
    public ResponseEntity<List<Category>> getCategories(){
        return ResponseEntity.ok(categoryService.getCategories());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Category> getCategory(@PathVariable("id") long catId){
        return ResponseEntity.ok(categoryService.getCategory(catId));
    }

    @PostMapping
    public ResponseEntity<Void> createCategory(@Valid @RequestBody Category category){
        categoryService.createCategory(category);
        URI uriLocation = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(category.getId())
                .toUri();
        return ResponseEntity.created(uriLocation).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updateCategory(@PathVariable("id") long id,
                                               @Valid @RequestBody Category category){
        categoryService.updateCategory(id, category);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCategory(@PathVariable("id") long id){
        categoryService.deleteCategory(id);
        return ResponseEntity.noContent().build();
    }
}
