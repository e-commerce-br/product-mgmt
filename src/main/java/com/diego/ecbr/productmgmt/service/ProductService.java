package com.diego.ecbr.productmgmt.service;

import com.diego.ecbr.productmgmt.dao.model.Product;

import java.util.List;

public interface ProductService {

    List<Product> getProducts();

    Product getProduct(long productId);

    Product createProduct(Product product);

    void updateProduct(long productId, Product product);

    void deleteProduct(long productId);
}
