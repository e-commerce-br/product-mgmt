package com.diego.ecbr.productmgmt.service.impl;

import com.diego.ecbr.productmgmt.dao.model.Category;
import com.diego.ecbr.productmgmt.dao.repository.CategoryRepository;
import com.diego.ecbr.productmgmt.service.CategoryService;
import com.diego.ecbr.productmgmt.service.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepo;

    @Override
    public Category getCategory(long catId) {
        return categoryRepo
                .findById(catId)
                .orElseThrow(() -> new ObjectNotFoundException("Category with provide id: " + catId + ", was not found."));
    }

    @Override
    public List<Category> getCategories() {
        return categoryRepo.findAll();
    }

    @Override
    public Category createCategory(Category category) {
        return categoryRepo.save(category);
    }

    @Override
    public void updateCategory(long categoryId, Category category) {
        // check category exists
        getCategory(categoryId);
        category.setId(categoryId);
        categoryRepo.save(category);
    }

    @Override
    public void deleteCategory(long categoryId) {
        Category category = getCategory(categoryId);
        categoryRepo.delete(category);
    }
}
