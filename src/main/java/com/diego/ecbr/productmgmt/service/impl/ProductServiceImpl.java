package com.diego.ecbr.productmgmt.service.impl;

import com.diego.ecbr.productmgmt.dao.model.Product;
import com.diego.ecbr.productmgmt.dao.repository.ProductRepository;
import com.diego.ecbr.productmgmt.service.ProductService;
import com.diego.ecbr.productmgmt.service.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepo;

    @Override
    public List<Product> getProducts() {
        return productRepo.findAll();
    }

    @Override
    public Product getProduct(long productId) {
        return productRepo
                .findById(productId)
                .orElseThrow(
                        () -> new ObjectNotFoundException("Product with provide id: " + productId + " was not found.")
                );
    }

    @Override
    public Product createProduct(Product product) {
        return productRepo.save(product);
    }

    @Override
    public void updateProduct(long productId, Product product) {
        getProduct(productId);
        product.setId(productId);
        productRepo.save(product);
    }

    @Override
    public void deleteProduct(long productId) {
        Product product = getProduct(productId);
        productRepo.delete(product);
    }
}
