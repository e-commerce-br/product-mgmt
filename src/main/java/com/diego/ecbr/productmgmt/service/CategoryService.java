package com.diego.ecbr.productmgmt.service;

import com.diego.ecbr.productmgmt.dao.model.Category;

import java.util.List;

public interface CategoryService {

    Category getCategory(long catId);

    List<Category> getCategories();

    Category createCategory(Category category);

    void updateCategory(long categoryId, Category category);

    void deleteCategory(long categoryId);
}
