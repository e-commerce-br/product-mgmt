package com.diego.ecbr.productmgmt.dao.repository;

import com.diego.ecbr.productmgmt.dao.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
