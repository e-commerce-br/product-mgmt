package com.diego.ecbr.productmgmt.dao.repository;

import com.diego.ecbr.productmgmt.dao.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
}
